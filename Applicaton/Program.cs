﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Console;

namespace Applicaton
{
    class Solution
    {
        public static string LongestWord(string sen)
        {
            string palabra = "";
            List<string> lista = new List<string>();
            foreach (char letra in sen)
            {
                if (letra != ' ' || sen.IndexOf(letra) == (sen.Length - 1))
                    palabra += letra;
                else
                {
                    lista.Add(palabra);
                    palabra = "";
                }
            }
            lista.Add(palabra);
            for (int x = 0; x < lista.Count; x++)
            {
                if (x != (lista.Count - 1))
                {
                    if (lista[x].Length > lista[(x + 1)].Length)
                    {
                        palabra = lista[x];
                        lista[x] = lista[(x + 1)];
                        lista[(x + 1)] = palabra;

                    }
                }
            }
            return lista[(lista.Count - 1)];
        }

        public static string FirstReverse(string srt)
        {
            /*ArrayList pepe = new ArrayList();
            pepe.Reverse();*/
            //Stack<string> s = new Stack<string>();
            string palabra = "";
            //palabra = srt.Reverse<char>().ToString();
            for (int x = srt.Length; x > 0; x--)
            {
                palabra += srt[(x - 1)];
            }
            return palabra;
        }

        public static string LetterChanges(string str)
        {
            List<char> abecedario = new List<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            int i = 0;
            string palabra = "";
            for (int x = 0; x < str.Length; x++)
            {
                str.ToLower();
                i = abecedario.IndexOf(str[x]);
                palabra += abecedario[(i + 1)];
            }
            return palabra;
        }

        public static int SimpleAdding(int num)
        {
            int suma = 0;
            for (int x = 1; x <= num; x++)
                suma += x;
            return suma;
        }

        public static string LetterCapitalize(string str)
        {
            string r = "";
            r += char.ToUpper(str[0]);
            for (int x = 1; x < str.Length; x++)
            {
                r += str[x];
                if (str[x] == ' ') //Considerar que puedan colocar espacios al final
                {
                    r += char.ToUpper(str[(x + 1)]);
                    x++;
                }
            }
            return r;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            WriteLine(Solution.LongestWord(ReadLine()));

            WriteLine(Solution.FirstReverse(ReadLine()));

            WriteLine(Solution.LetterChanges(ReadLine()));

            Write(Solution.SimpleAdding(int.Parse(ReadLine())));

            WriteLine(Solution.LetterCapitalize(ReadLine()));
            ReadKey();
        }
    }
}
